package application;
	
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Stack;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;


public class Main extends Application {
	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("/View/Admin/Login.fxml"));
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
			//Queue a ={10,9,8,7,6,5,4,3,2,1};
			Queue<Integer> a = new LinkedList<Integer>();
			a.add(10);
			a.add(9);
			a.add(8);
			a.add(7);
			a.add(6);
			a.add(5);
			a.add(4);
			a.add(3);
			a.add(2);
			a.add(1);
			Queue q = interChanger(a);
			
		
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
	public Queue interChanger(Queue q){
		 Stack<Integer> st = new Stack<Integer>(); 
		 int size = q.size()/2;
		 for(int i = 1; i <= size; i++){ 
		  st.push((Integer) q.remove());  //1. ____ 
		 } 
		 while(!st.isEmpty()){ 
		  q.add(st.pop());      //2. ____ 
		  
		 } 
		 
		 for(int i = 1; i <= size; i++){ 
		  q.add(q.remove());    //3. ____ 
		 }
		 
		 for(int i = 1; i <= size; i++){
		  st.push((Integer) q.remove());  //4. ____ 
		 } 
		 System.out.println(q);
		 while(!st.isEmpty()){
		  q.add(st.pop());      
		  q.add(q.remove());   //5. ____ 
		 }     
		 
		 return q;
}
}
